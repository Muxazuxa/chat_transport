import discord
from base import ChatTransport

class ChatTransportDiscord(ChatTransport):
    def __init__(self, token):
        self.client = discord.Client()
        self.token = token

    def add_handler(self, event):
        self.client.event(event)

    async def send_message(self, msg):
        # Implement sending message logic
        pass

    async def run(self):
        await self.client.start(self.token)
