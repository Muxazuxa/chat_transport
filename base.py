from abc import ABC, abstractmethod

class ChatTransport(ABC):
    @abstractmethod
    def add_handler(self, event):
        pass

    @abstractmethod
    async def send_message(self, msg):
        pass

    @abstractmethod
    async def run(self):
        pass
