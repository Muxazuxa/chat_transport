from telegram import Bot
from telegram.ext import Updater
from chat_transport import ChatTransport

class ChatTransportTelegram(ChatTransport):
    def __init__(self, token):
        self.bot = Bot(token)
        self.updater = Updater(bot=self.bot, use_context=True)

    def add_handler(self, event):
        self.updater.dispatcher.add_handler(event)

    async def send_message(self, msg):
        # Implement sending message logic
        pass

    async def run(self):
        self.updater.start_polling()
