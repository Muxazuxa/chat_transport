class SimpleBusinessLogicBot:
    def __init__(self, transport):
        self.transport = transport
        self.transport.add_handler(self.handle_message)

    async def handle_message(self, message):
        await self.transport.send_message(f"Hi! Your message was received: {message}")

    async def run(self):
        await self.transport.run()
