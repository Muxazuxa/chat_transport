from business_logic import SimpleBusinessLogicBot

from discord_transport import ChatTransportDiscord

bot = SimpleBusinessLogicBot(ChatTransportDiscord("your_discord_token"))
# or
# bot = SimpleBusinessLogicBot(ChatTransportTelegram("your_telegram_token"))


if __name__ == '__main__':
    await bot.run()

